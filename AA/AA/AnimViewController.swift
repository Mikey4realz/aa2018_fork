//
//  AnimViewController.swift
//  AA
//
//  Created by CSE User on 2/28/18.
//  Copyright © 2018 Michael Strade. All rights reserved.
//

import UIKit

class AnimViewController: UIViewController {
    
    @IBOutlet weak var brick: UIImageView!
    @IBOutlet weak var brick02: UIImageView!
    @IBOutlet weak var brick03: UIImageView!
    @IBOutlet weak var brick04: UIImageView!
    @IBOutlet weak var brick05: UIImageView!
    @IBOutlet weak var brick06: UIImageView!
    @IBOutlet weak var brick07: UIImageView!
    @IBOutlet weak var brick08: UIImageView!
    @IBOutlet weak var brick09: UIImageView!
    @IBOutlet weak var brick010: UIImageView!
    @IBOutlet weak var brick011: UIImageView!
    @IBOutlet weak var brick012: UIImageView!
    @IBOutlet weak var brick013: UIImageView!
    @IBOutlet weak var brick014: UIImageView!
    @IBOutlet weak var brick015: UIImageView!
    @IBOutlet weak var brick016: UIImageView!
    @IBOutlet weak var brick017: UIImageView!
    @IBOutlet weak var brick018: UIImageView!
    @IBOutlet weak var brick019: UIImageView!
    @IBOutlet weak var brick020: UIImageView!
    @IBOutlet weak var brick021: UIImageView!
    @IBOutlet weak var brick022: UIImageView!
    @IBOutlet weak var brick023: UIImageView!
    @IBOutlet weak var brick024: UIImageView!
    @IBOutlet weak var brick025: UIImageView!
    @IBOutlet weak var brick026: UIImageView!
    @IBOutlet weak var brick027: UIImageView!
    @IBOutlet weak var brick028: UIImageView!
    @IBOutlet weak var brick029: UIImageView!
    @IBOutlet weak var brick030: UIImageView!
    @IBOutlet weak var brick031: UIImageView!
    @IBOutlet weak var brick032: UIImageView!
    @IBOutlet weak var brick033: UIImageView!
    @IBOutlet weak var brick034: UIImageView!
    @IBOutlet weak var brick035: UIImageView!
    @IBOutlet weak var brick036: UIImageView!
    @IBOutlet weak var brick037: UIImageView!
    @IBOutlet weak var brick038: UIImageView!
    @IBOutlet weak var brick039: UIImageView!
    @IBOutlet weak var brick040: UIImageView!
    @IBOutlet weak var brick041: UIImageView!
    @IBOutlet weak var brick042: UIImageView!
    @IBOutlet weak var brick043: UIImageView!
    @IBOutlet weak var brick044: UIImageView!
    @IBOutlet weak var brick045: UIImageView!
    @IBOutlet weak var brick046: UIImageView!
    @IBOutlet weak var brick047: UIImageView!
    @IBOutlet weak var brick048: UIImageView!
    @IBOutlet weak var brick049: UIImageView!
    @IBOutlet weak var dk: UIImageView!
    
    @IBOutlet weak var codeBox: UITextView!
    @IBOutlet weak var outBox: UITextView!
    
    var lineRanges = [NSRange]()
    var textStorage = NSMutableAttributedString()
    
    lazy var brickArray = [brick,brick02,brick03,brick04,brick05,brick06,brick07,brick08,brick09,brick010,brick011,brick012,brick013,brick014,brick015,brick016,brick017,brick018,brick019,brick020,brick021,brick022,brick023,brick024,brick025,brick026,brick027,brick028,brick029,brick030,brick031,brick032,brick033,brick034,brick035,brick036,brick037,brick038,brick039,brick040,brick041,brick042,brick043,brick044,brick045,brick046,brick047,brick048,brick049,dk]
    
    @IBOutlet weak var AnimationImageView: UIImageView!
    var attrA: Int!
    var attrB: Int!
    var constType: String!
    var range: Int!
    var counter: Bool!
    var time: Int = 200
    var scalar: Int!
    var end: Bool = false
    var once: Bool = false
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    
    override func viewDidLoad() {
        if(attrB>attrA) {
            if attrB - attrA > 50{
                attrB = attrA + 50
            }
            range = Int(attrB! - attrA!)
            //counter = true means increment
            counter = true
        }
        else if(attrB<attrA) {
            if attrA - attrB > 50{
                attrB = attrA - 50
            }
            range = Int(attrA! - attrB!)
            //counter = false means decrement
            counter = false
        }
        
        super.viewDidLoad()
        clearBricks()
        
        outBox.text = ""
        
        //Change conditions based on construct type for
        if constType == "for"{
            scalar = 3
            codeBox.text = "for(i in A to B) do\n\tprint(i)\n\tcreateBrick()"
            self.scheduleHighlighting(ofLine: 0)
            delay(A: 0, B: range, Current: attrA)
        }
        
        
        //Change conditions based on construct type for
        if constType == "while"{
            scalar = 4
            codeBox.text = "while(A < B) do\n\tprint(A)\n\tcreateBrick()\n\tA = A + 1"
            self.scheduleHighlighting(ofLine: 0)
            delay(A: 0, B: range, Current: attrA)
        }
        
        //Change conditions based on construct type for
        if constType == "if"{
            //TODO: Implement if statement and animation
        }
        
        // Do any additional setup after loading the view.
    }
    
    func highlight() {
        
        textStorage = codeBox.textStorage
        
        let storageString = textStorage.string as NSString
        storageString.enumerateSubstrings(in: NSMakeRange(0, storageString.length), options: .byLines, using: { (_, lineRange, _, _) in
            self.lineRanges.append(lineRange)
        })
        
    }
    
    func setBackgroundColor(_ color: UIColor?, forLine line: Int) {
        if let color = color {
            textStorage.addAttribute(.backgroundColor, value: color, range: lineRanges[line])
        } else {
            textStorage.removeAttribute(.backgroundColor, range: lineRanges[line])
        }
    }
    
    func scheduleHighlighting(ofLine line: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(self.time)) {
            if !self.once {
                self.once = true
                self.scheduleHighlighting(ofLine: 0)
                
            }
            if self.end && line == self.lineRanges.count {
                return
            }
            if line > 0 {
                self.setBackgroundColor(nil, forLine: line - 1)
                
            }
            if line >= self.lineRanges.count {
                self.highlight()
            }
            self.setBackgroundColor(.yellow, forLine: line)
            self.scheduleHighlighting(ofLine: line + 1)
        }
    }

    
    func delay(A: Int, B: Int, Current: Int){
        if(A>=B){
            self.end = true
            return
        }
        else{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(scalar*time)){
                self.outBox.text = self.outBox.text + "\(Current)\n"
                self.outBox.simple_scrollToBottom()
                self.brickArray[A]?.isHidden=false
                if self.counter == true{
                    self.delay(A: A+1, B: B, Current: Current+1)
                }
                else if self.counter == false{
                    self.delay(A: A+1, B: B, Current: Current-1)
                }
            }
            return
        }
    }
    
    func clearBricks(){
        var count=0
        while(brickArray.index(after: (count-1)) != brickArray.endIndex){
            self.brickArray[count]?.isHidden=true
            count=count+1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextView {
    func simple_scrollToBottom() {
        let textCount: Int = text.count
        guard textCount >= 1 else { return }
        scrollRangeToVisible(NSMakeRange(textCount - 1, 1))
    }
}
