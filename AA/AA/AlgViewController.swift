//
//  AlgViewController.swift
//  AA
//
//  Created by CSE User on 2/19/18.
//  Copyright © 2018 Michael Strade. All rights reserved.
//

import UIKit
import Foundation

class CustomUITextField: UITextField {
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste(_:)) {
            return false
        }
        
        return true
    }
}

class AlgViewController: UIViewController {
    
    @IBOutlet weak var ConstructTypeLabel: UILabel!
    @IBOutlet weak var ForConstructButton: UIBarButtonItem!
    @IBOutlet weak var AttrBTextField: CustomUITextField!
    @IBOutlet weak var AttrATextField: CustomUITextField!
    @IBOutlet weak var AnimButton: UIButton!
    @IBOutlet weak var AttrALabel: UILabel!
    @IBOutlet weak var AttrBLabel: UILabel!
    @IBOutlet weak var CodeTextView: UITextView!
    @IBOutlet weak var AlgTextView: UITextView!
    var constructType: String!
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    @IBAction func ForConstructClick(_ sender: Any) {
        ConstructTypeLabel.text = "for()"
        AnimButton.isEnabled = false
        self.AttrATextField.becomeFirstResponder()
        constructType = "for"
    }
    
    @IBAction func WhileButtonClick(_ sender: UIBarButtonItem) {
        ConstructTypeLabel.text = "while()"
        AnimButton.isEnabled = false
        self.AttrALabel.becomeFirstResponder()
        constructType = "while"
    }
    
    @IBAction func IfButtonClick(_ sender: UIBarButtonItem) {
        ConstructTypeLabel.text = "if()"
        AnimButton.isEnabled = false
        self.AttrALabel.becomeFirstResponder()
        constructType = "if"
    }
    
    @IBAction func RunButtonClick(_ sender: Any) {
        self.resignFirstResponder()
        AlgTextView.text = ""
        if(ConstructTypeLabel.text == "for()") {
            CodeTextView.text = ""
            guard let textA = AttrATextField.text,
                let textB = AttrBTextField.text,
                !textA.isEmpty || !textB.isEmpty else {
                    return
            }
            
            var A = (textA as NSString).integerValue
            var B = (textB as NSString).integerValue
            
            if( A < B) {
                if(B - A > 50) {
                    CodeTextView.text = "for() limited to a loop of up to 50\n\n\n"
                    B = A + 50
                }
                CodeTextView.text = CodeTextView.text! + "for(i in A to B) do\n\tprint(i)\nend for"
                for i in A..<B {
                    let index = "\(i)"
                    AlgTextView.text = AlgTextView.text! + "\(index)\n"
                }
                
                AnimButton.isEnabled = true
                
            }else if(B < A){
                if(A - B > 50) {
                    CodeTextView.text = "for() limited to a loop of up to 50\n\n\n"
                    B = A - 50
                }
                CodeTextView.text = CodeTextView.text! + "for(i in A to B) do\n\tprint(i)\nend while"
                
                while(A > B) {
                    let index = "\(A)"
                    AlgTextView.text = AlgTextView.text! + "\(index)\n"
                    A = A - 1
                }
                AnimButton.isEnabled = true
                
            }
            else {
                CodeTextView.text = "The parameter A must be less than parameter B!"
            }
            
        } else if(ConstructTypeLabel.text == "if()") {
            CodeTextView.text = ""
            
            guard let textA = AttrATextField.text,
                let textB = AttrBTextField.text,
                !textA.isEmpty || !textB.isEmpty else {
                    return
            }
            
            let A = (textA as NSString).integerValue
            let B = (textB as NSString).integerValue
            
            CodeTextView.text = "if(A < B) do\n\tprint(\"true\")\nelse\n\tprint(\"false\")\nend if"
            
            if( A < B) {
                AlgTextView.text = "true"
            }else{
                AlgTextView.text = "false"
                
            }
            AnimButton.isEnabled = true
            
        } else if (ConstructTypeLabel.text == "while()") {
            CodeTextView.text = ""
            
            guard let textA = AttrATextField.text,
                let textB = AttrBTextField.text,
                !textA.isEmpty || !textB.isEmpty else {
                    return
            }
            
            var A = (textA as NSString).integerValue
            var B = (textB as NSString).integerValue
            
            if(A < B) {
                if(B - A > 50) {
                    CodeTextView.text = "while() limited to a loop of up to 50\n\n\n"
                    B = A + 50
                }
                CodeTextView.text = CodeTextView.text! + "while(A < B) do\n\tprint(A)\n\tA = A + 1\nend while"
            
                while(A < B) {
                    let index = "\(A)"
                    AlgTextView.text = AlgTextView.text! + "\(index)\n"
                    A = A + 1
                }
                AnimButton.isEnabled = true
            } else if(B < A){
                if(A - B > 50) {
                    CodeTextView.text = "while() limited to a loop of up to 50\n\n\n"
                    B = A - 50
                }
                CodeTextView.text = CodeTextView.text! + "while(A > B) do\n\tprint(B)\n\tA = A - 1\nend while"
                
                while(A > B) {
                    let index = "\(A)"
                    AlgTextView.text = AlgTextView.text! + "\(index)\n"
                    A = A - 1
                }
                AnimButton.isEnabled = true
            }else{
                CodeTextView.text = "Values must be numbers and must not be equal!"
            }
        } else {
            CodeTextView.text = "Click a type of construct first from the bottom row to continue!"
        }
    }
    
    @IBAction func AnimButtonClick(_ sender: UIButton) {
        self.RunButtonClick([])
        let a = ConstructTypeLabel.text
        
        if(a?.isEmpty == false) {
            let anim = AnimViewController()
            anim.attrA = Int(AttrATextField.text!)
            anim.attrB = Int(AttrBTextField.text!)
            //anim.constType = ConstructTypeLabel.text
        }

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "animation" {
            let theDestination = (segue.destination as! AnimViewController)
            theDestination.constType = constructType
            theDestination.attrA = Int(AttrATextField.text!)
            theDestination.attrB = Int(AttrBTextField.text!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   // override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // }
    

}
